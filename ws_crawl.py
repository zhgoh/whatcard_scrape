from contextlib import closing
import json
import re
import websocket

class JSONError(Exception):
    pass


def get(json_data, tag):
    if tag in json_data:
        return json_data[tag]
    raise JSONError(f"Cannot find {tag} in json {json_data}")


def send(conn, msg):
    msg = json.dumps(msg)
    conn.send(msg)
    return json.loads(conn.recv())


def fetch_business():
    business = []
    # Most of these gotton from studying interactions with burpsuite
    # Note: Previously wasn't working (getting 400 error because was using ws instead of wss protocol)
    URL = "wss://www.whatcard.sg:443/live/websocket?vsn=2.0.0"
    with closing(websocket.create_connection(URL)) as conn:
        # Default for 1 is phx_join message
        msg = ["1","1","lv:phx-3WlRk0b0","phx_join",
        {
            "url":"https://www.whatcard.sg/",
            "params":{},
            "session":"SFMyNTY.g3QAAAACZAAEZGF0YWgCYQF0AAAABWQAAmlkbQAAAAxwaHgtM1dsUmswYjBkAApwYXJlbnRfcGlkZAADbmlsZAAGcm91dGVyZAAZRWxpeGlyLldoYXRjYXJkV2ViLlJvdXRlcmQAB3Nlc3Npb250AAAAAWQAC3Jld2FyZF90eXBlZAADbmlsZAAEdmlld2QAJUVsaXhpci5XaGF0Y2FyZFdlYi5NZXJjaGFudExpdmUuSW5kZXhkAAZzaWduZWRuBgC1DvEPfwE.o9_g4otcUII7vX4WmlC10L5yJEko78a1H8X8A0CyAcM",
            "static": None
        }]

        send(conn, msg)

        counter = 2
        while True:
            print(f"Sending request {counter}\r", end="")
            # Subsequent message is event
            msg = ["1", f"{counter}", "lv:phx-3WlRk0b0", "event",
            {
                "type":"click",
                "event":"load-more",
                "value": ""
            }]

            # Try to send, when it fails, it has reach the max
            try:
                results = send(conn, msg)
                if results[3] == "phx_reply":
                    # Expecting diff instead of rendered tag
                    dynamics = get(get(get(get(results[4], "response"), "diff"), "19"), "dynamics")
                else:
                    break
                counter += 1
            except JSONError:
                print(f"Stopped at {counter}")
                for item in dynamics:
                    name = item[0].strip()
                    p = re.compile("(?:.*\n)*\s*(.*){1}(?:\n)")
                    match = p.search(name)
                    if match:
                        name = match.group(1)
                    mcc  = item[1].strip()
                    business.append(f'{name}, {mcc}\n')
                break
    return business


def save(file, lines):
    with open(file, mode='wt', encoding='utf8') as f:
        f.write("name, mcc")
        f.writelines(lines)
    print("Written to file")


#websocket.enableTrace(True)
save("results.csv", fetch_business())

'''
Notes:
- Initially getting 400 error because I was using ws instead of wss (secure vs non-secure link)
- Looks like using phoenix framework for backend
- Heartbeat sent every 30s
- Initially using multi-threaded code to send heartbeat every 30s, removed that as I am sending request till I get all the business
- Initially written an async version, that utilized asyncio sleep for sending heartbeat but since scraped that
- Looks like heartbeat is not needed as long as I keep sending request to the endpoint
'''
